package com.example.drunkman

import android.bluetooth.BluetoothSocket
import android.util.Log
import com.example.drunkman.connection.ConnectionFragment

/**
 * Class to controll the information flow between clients
 */
class SocketManager(private val connectionFragment: ConnectionFragment) : Thread() {
    private val socketList: MutableList<BluetoothSocket>
    private var isCanceled: Boolean

    private val TAG = "SocketManager" //Debuging

    init {
        connectionFragment.appendText("SocketManager: Create")
        Log.i(TAG, "Create")
        socketList = mutableListOf()
        isCanceled = false
    }

    override fun run() {
        while (!isCanceled) {
            if (socketList.size > 0) {
                for (socket in socketList) {
                    val inputStream = socket.inputStream
                    val available = inputStream.available()
                    if (available > 0) {
                        Log.i(TAG, "got something")
                        val byte = ByteArray(available)
                        inputStream.read(byte, 0, available)
                        broadCastMessage(String(byte))
                        connectionFragment.appendText(String(byte))
                    }
                }
            }
        }
        if (isCanceled) {
            connectionFragment.appendText("SocketManager: Close")
            Log.i(TAG, "Close Manager")
        }
    }

    fun addSocket(socket: BluetoothSocket) {
        Log.i(TAG, "added Socket")
        socketList.add(socket)
    }

    fun broadCastMessage(msg: String) {
        for (socket in socketList) {
            val outputStream = socket.outputStream
            outputStream.write(msg.toByteArray())
            outputStream.flush()
        }
    }

    fun cancel() {
        isCanceled = true
    }

}