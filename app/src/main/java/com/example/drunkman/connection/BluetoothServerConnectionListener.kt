package com.example.drunkman.connection

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import android.content.Intent
import android.util.Log
import java.io.IOException
import java.util.*

/**
 * Waiting for the connection from clients and creates the connectionserver
 */
class BluetoothServerConnectionListener(
    uuid: UUID,
    private val connectionFragment: ConnectionFragment,
    private val btAdapter: BluetoothAdapter?
) : Thread() {
    private var cancelled: Boolean
    private val serverSocket: BluetoothServerSocket?

    init {
        Log.i("BluetoothServerConnectionListener", "Create")
        connectionFragment.appendText("BluetoothServerConnectionListener: Create")
        if (btAdapter != null) {
            //Visiablility in Bluetooth
            val discoverableIntent: Intent =
                Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE).apply {
                    putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300)
                }
            connectionFragment.startActivity(discoverableIntent)
            //Start ServerSocket
            this.serverSocket = btAdapter.listenUsingRfcommWithServiceRecord("Drunkman", uuid)
            this.cancelled = false
        } else {
            this.serverSocket = null
            this.cancelled = true
        }
    }

    override fun run() {
        var socket: BluetoothSocket

        while (true) {
            if (this.cancelled) {
                serverSocket!!.close()
                return
            }

            try {
                Log.i("server", "Waiting")
                socket = serverSocket!!.accept()

            } catch (e: IOException) {
                Log.i("ServerControll", "Error")
                break
            }

            if (!this.cancelled && socket != null) {
                Log.i("server", "Connecting")
                ConnectionServer(
                    this.connectionFragment.getSocketManager(),
                    socket,
                    btAdapter!!
                ).start()
            }
        }
    }

    fun cancel() {
        cancelled = true
        serverSocket?.close()
        connectionFragment.appendText("BluetoothServerConnectionListener: Close")
        Log.i("ServerController", "Close Controller")
    }

}