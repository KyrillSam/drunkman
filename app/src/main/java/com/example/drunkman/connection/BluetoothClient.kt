package com.example.drunkman.connection

import android.bluetooth.BluetoothDevice
import android.util.Log
import java.util.*

/**
 * Clientclass for the communication
 */
class BluetoothClient(
    private val device: BluetoothDevice,
    uuid: UUID,
    private val connectionFragment: ConnectionFragment
) : Thread() {
    private var socket = device.createRfcommSocketToServiceRecord(uuid)
    private val newUUID = UUID.randomUUID()
    private var canceled = false
    private var msg = ""
    val TAG: String = "BluetoothClient"

    override fun run() {
        try {
            Log.i(TAG, "Connecting")
            connectionFragment.appendText("Connecting to ${device.name}")
            this.socket.connect()
        } catch (e: java.lang.Exception) {
            Log.i(TAG, "TimeOut")
            connectionFragment.appendText("TimeOut")
        }
        if (socket.isConnected) {
            var outputStream = this.socket.outputStream
            var inputStream = this.socket.inputStream
            try {
                Log.i(TAG, "Sending: $newUUID")
                connectionFragment.appendText("Sending: $newUUID")
                outputStream.write(newUUID.toString().toByteArray())
                outputStream.flush()
                Log.i(TAG, "Sent the UUID")
            } catch (e: Exception) {
                Log.e(TAG, "Cannot send", e)
                connectionFragment.appendText("Failed sending")
            }

            //Waiting for Socket being created
            var available = inputStream.available()
            Log.i(TAG, "Waiting for ACCEPT")
            connectionFragment.appendText("Waiting for Accept from Server ...")
            while (available < 1) {
                available = inputStream.available()
            }
            val byte = ByteArray(available)
            inputStream.read(byte, 0, available)
            Log.i(TAG, String(byte))

            //Individual Socket Created
            outputStream.close()
            inputStream.close()
            socket.close()
            val newSocket = device.createRfcommSocketToServiceRecord(newUUID)
            newSocket.connect()
            Log.i(TAG, "newSocket Connected: ${newSocket.isConnected}")
            connectionFragment.appendText("Connected to server: ${newSocket.isConnected}")
            outputStream = newSocket.outputStream
            inputStream = newSocket.inputStream
            Log.i(TAG, "Connected To My Socket")

            //While the Conection is alive
            while (!canceled) {
                available = inputStream.available()
                if (available > 0) {
                    val incByte = ByteArray(available)
                    inputStream.read(incByte, 0, available)
                    connectionFragment.appendText(String(incByte))
                }
                if (msg != "") {
                    Log.i(TAG, "Sending: $msg")
                    outputStream.write(msg.toByteArray())
                    outputStream.flush()
                    msg = ""
                }
            }
        }
    }

    fun wrtieMessage(str: String) {
        this.msg = str
    }
}