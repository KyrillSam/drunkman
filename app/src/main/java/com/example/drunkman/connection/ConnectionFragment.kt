package com.example.drunkman.connection

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.drunkman.*
import com.example.drunkman.ui.MyAdapter
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.main_menu.*
import kotlinx.android.synthetic.main.main_menu.view.*
import kotlinx.android.synthetic.main.mainframe.*
import java.util.*

private const val REQUEST_CODE = 999
private val comChannel: UUID = UUID.fromString("40e50f7a-6964-11ea-bc55-0242ac130003")

/**
 * MainMenu view
 */
class ConnectionFragment(private val activity: MainActivity) : Fragment() {
    private val btAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()
    private var socketManager: SocketManager? = null
    private lateinit var inPut: EditText
    private lateinit var console: TextView
    private lateinit var pairedDevices: Set<BluetoothDevice>
    private var client: BluetoothClient? = null
    private var hosting: Boolean = false
    private var serverController: BluetoothServerConnectionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.main_menu, container, false)
        this.inPut = root.inPut
        this.console = root.consoleField
        console.movementMethod = ScrollingMovementMethod()

        if (btAdapter == null) {
            val snack = Snackbar.make(activity.root_layout, "No Bluetooth", Snackbar.LENGTH_LONG)
            snack.show()
        } else {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_CODE)
        }

        root.button.setOnClickListener {
            if (client != null && client!!.isAlive && !hosting) {
                client!!.wrtieMessage(inPut.text.toString())
                inPut.text.clear()
            } else if (hosting) {
                this.appendText(inPut.text.toString())
                socketManager!!.broadCastMessage(inPut.text.toString())
                inPut.text.clear()
            } else {
                Log.i("MainActivity", "No Client")
            }
        }

        root.switchHost.setOnClickListener {
            if (switchHost.isChecked) {
                hosting = true
                switchHost.setTextColor(Color.GREEN)
                serverController =
                    BluetoothServerConnectionListener(
                        comChannel,
                        this,
                        btAdapter!!
                    )
                serverController!!.start()
                socketManager = SocketManager(this)
                socketManager!!.start()
            } else {
                hosting = false
                switchHost.setTextColor(Color.BLACK)
                serverController!!.cancel()
                socketManager!!.cancel()
            }
        }
        return root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE && resultCode == AppCompatActivity.RESULT_OK) {
            super.onActivityResult(requestCode, resultCode, data)
            //Manage the RecycleView
            val man = LinearLayoutManager(activity)
            pairedDevices = btAdapter!!.bondedDevices
            val name: Array<String?> = arrayOfNulls(pairedDevices.size)
            var i = 0
            pairedDevices.forEach { device ->
                val deviceName = device.name
                //val deviceHardwareAddress = device.address // MAC addres
                name[i++] = deviceName
            }
            val adapRec = MyAdapter(name, this)
            this.recycleView.apply {
                setHasFixedSize(true)
                layoutManager = man
                adapter = adapRec
            }
        }

    }

    fun findName(s: String) {
        pairedDevices.forEach { device ->
            if (device.name.equals(s)) {
                Log.i("MainActivity: Searching Device", device.address.toString())
                client = BluetoothClient(
                    device,
                    comChannel,
                    this
                )
                client!!.start()
                activity.setClient(client!!)
                return
            }
        }
        Log.d("Suche", "Not Found")
    }

    fun appendText(text: String) {
        Log.d("write", text)
        activity.runOnUiThread {
            console.append(text + "\n")
            console.scrollTo(0,console.lineCount)
        }
    }

    fun getSocketManager(): SocketManager {
        return this.socketManager!!
    }
}