package com.example.drunkman.connection

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothSocket
import android.util.Log
import com.example.drunkman.SocketManager

/**
 * Main connection server to ask for personal socket/connection
 */
class ConnectionServer(
    private val socketsManager: SocketManager?, private val socket: BluetoothSocket,
    private val btAdapter: BluetoothAdapter
) : Thread() {
    private val inputStream = this.socket.inputStream
    private val outputStream = this.socket.outputStream

    override fun run() {
        var available = 0
        Log.i("ConnectionServer", "Listening to Connection")
        try {
            while (available <= 0) {
                available = inputStream.available()
            }
            val bytes = ByteArray(available)
            Log.i("server", "Reading")
            inputStream.read(bytes, 0, available)
            val text = String(bytes)
            Log.i("server", "Message received")
            Log.i("server", text)

            val sc = SocketCreater(text, btAdapter)
            sc.start()
            outputStream.write("ACCEPTED".toByteArray())
            outputStream.flush()
            Log.i("ConnectionServer", "Ok Send")
            var gotSocket = false
            while (!gotSocket) {
                if (sc.getSocket() != null && !sc.isRunning()) {
                    Log.i("ConnectionServer", "Erreicht")
                    socketsManager!!.addSocket(sc.getSocket()!!)
                    inputStream.close()
                    outputStream.close()
                    socket.close()
                    gotSocket = true
                }
            }

        } catch (e: Exception) {
            Log.e("client", "Cannot read data", e)
        }

    }
}