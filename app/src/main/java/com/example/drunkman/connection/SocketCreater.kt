package com.example.drunkman.connection

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import android.util.Log
import java.io.IOException
import java.util.*

/**
 * Class to create individual sockets for each client
 */
class SocketCreater(str: String, btAdapter: BluetoothAdapter) : Thread() {
    private var socket: BluetoothSocket?
    private val serverSocket: BluetoothServerSocket
    private var isRunning: Boolean

    init {
        socket = null
        isRunning = false
        Log.i("SocketCreater", "Create: $str")
        serverSocket =
            btAdapter.listenUsingRfcommWithServiceRecord("Unsee_this", UUID.fromString(str))
    }

    override fun run() {
        isRunning = true
        while (socket == null) {
            try {
                Log.i("SocketCreater", "Waiting")
                socket = serverSocket!!.accept(3000)
                Log.i("SocketCreater", "Connected")
                if (socket == null) {
                    Log.i("SocketCreater", "Timeout")
                } else {
                    Log.i("SocketCreater", "created socket")
                }
            } catch (e: IOException) {
                Log.i("SocketCreater", "Error")
                break
            }
        }
        isRunning = false

    }

    fun getSocket(): BluetoothSocket? {
        return socket
    }

    fun isRunning(): Boolean {
        return isRunning
    }
}